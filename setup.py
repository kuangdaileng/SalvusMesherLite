#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
from setuptools import find_packages, setup
from setuptools.extension import Extension
from setuptools.command.build_py import build_py

import fnmatch
import inspect
import multiprocessing
import os
import pathlib
import sys

DOCSTRING = __doc__.strip().split("\n")


root_dir = pathlib.Path(inspect.getfile(inspect.currentframe())).parent

# Import the version string.
sys.path.insert(0, str((root_dir / "salvus_mesh_lite").absolute()))
from version import get_git_version  # NOQA


def get_package_data():
    """
    Returns a list of all files needed for the installation relativ to the
    "salvus_mesh_lite" subfolder.
    """
    filenames = []
    root_dir = pathlib.Path(inspect.getfile(inspect.currentframe())).parent
    root_dir = str(root_dir / "salvus_mesh_lite")
    # Recursively include all files in these folders:
    folders = [
        # Data
        os.path.join(root_dir, "data"),
        # input schema
        os.path.join(root_dir, "interface", "schemas"),
        # test data.
        os.path.join(root_dir, "tests", "data"),
        # Shaders.
        os.path.join(root_dir, "visualization", "jupyter_widgets",
                     "mesh_widget", "shaders")
    ]
    for folder in folders:
        for directory, _, files in os.walk(folder):
            for filename in files:
                # Exclude hidden files.
                if filename.startswith("."):
                    continue
                filenames.append(os.path.relpath(
                    os.path.join(directory, filename),
                    root_dir))
    filenames.append("RELEASE-VERSION")
    return filenames


# The first extension is the actual C code.
src = os.path.join('salvus_mesh_lite', 'src')
lib = Extension('salvus_mesh_lite.lib.salvus_mesh_lite',
                sources=[
                    os.path.join(src, "angle.c"),
                    os.path.join(src, "connectivity.c"),
                    os.path.join(src, "centroid.c"),
                    os.path.join(src, "hmin_max.c"),
                    os.path.join(src, "facets.c"),
                    os.path.join(src, "sort.c")],
                libraries=["m"])
extensions = [lib]



INSTALL_REQUIRES = ["numpy",
                    "scipy",
                    "matplotlib",
                    "lxml",
                    "jsonschema",
                    "h5py",
                    "pyexodus",
                    "pyyaml",
                    "pytest",
                    "flake8",
                    "pytest-mpl"]





setup_config = dict(
    name="salvus_mesh_lite",
    version=get_git_version(),
    description=DOCSTRING[0],
    long_description="\n".join(DOCSTRING[2:]),
    author="Mondaic AG",
    author_email="info@mondaic.com",
    url="https://www.mondaic.com",
    packages=find_packages(),
    package_data={
        "salvus_mesh_lite":
            [os.path.join("lib", "salvus_mesh_lite.so")] +
            get_package_data()},
    license="Proprietary",
    platforms="OS Independent",
    install_requires=INSTALL_REQUIRES,
    # ext_package='salvus_mesh_lite.lib',
    ext_modules=extensions,
    classifiers=[
        # complete classifier list:
        # http://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: MacOS",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Physics"
        ],
    zip_safe=False
)


if __name__ == "__main__":
    setup(**setup_config)