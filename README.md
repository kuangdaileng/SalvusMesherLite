# SalvusMesh Lite

This package contains the GPLv3 licensed lite version of the SalvusMesh
package. It is capable of generating meshes with all the bells and whistles
for usage in AxiSEM3D. If you are looking for the full version of the mesher
head over to http://mondaic.com.

## Installation

Trivial with `conda`:

```bash
$ conda install -c conda-forge salvus_mesh_lite
```
Note that we renamed the package to `salvus_mesh_lite` to be in line with other
salvus packages, but did not yet release a new conda package. So if you use the
conda version use `salvus_mesher_lite` in all commands and if you install the
most recent version from this repository use `salvus_mesh_lite`.

## Usage

Two steps:

```bash
$ python -m salvus_mesher_lite.interface AxiSEM --save_yaml=input.yaml
```

This will create a self-explanatory `input.yaml` YAML file. Edit it to your
satisfaction and run

```bash
$ python -m salvus_mesher_lite.interface --input_file=input.yaml
```