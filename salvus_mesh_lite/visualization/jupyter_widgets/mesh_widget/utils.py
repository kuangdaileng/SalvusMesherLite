#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
import io

import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np


def get_colormap_image(value_range: (float, float),
                       colormap: matplotlib.colors.Colormap):
    """
    Helper function returning a byte string plotting a colormap.
    """
    plt.close("all")
    gradient = np.linspace(0, 1, 256)
    gradient = np.tile(gradient.T, (1, 1))

    plt.figure(figsize=(1.5, 6), dpi=100)

    plt.imshow(gradient.T, aspect='auto', cmap=colormap)

    ticks = np.linspace(0, 256, 10)
    labels = np.linspace(value_range[0], value_range[1], len(ticks))
    labels = [f"{i:g}" for i in labels]
    plt.yticks(np.linspace(0, 256, 10), labels)
    plt.ylim(0, 256)

    plt.tick_params(
        axis='x',
        which='both',
        bottom=False,
        top=False,
        labelbottom=False)

    plt.tight_layout()

    with io.BytesIO() as buf:
        plt.savefig(buf, format="png", dpi=100)
        buf.seek(0, 0)
        value = buf.read()
    plt.close("all")

    return value