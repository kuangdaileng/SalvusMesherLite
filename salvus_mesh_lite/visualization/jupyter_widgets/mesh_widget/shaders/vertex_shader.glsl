attribute vec3 center;
attribute vec4 colors;
varying vec3 vCenter;
varying vec4 vColors;

void main() {
    vCenter = center;
    vColors = colors;

    gl_Position = projectionMatrix * modelViewMatrix *
        vec4(position, 1.0);
}