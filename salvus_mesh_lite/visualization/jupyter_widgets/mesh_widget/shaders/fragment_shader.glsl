/*
Shader coloring the edges by computing the distance to the next closest edge
in the fragment shader using barycentric coordinates.

Relies on the center and colors from the vertex shader.
*/
varying vec3 vCenter;
varying vec4 vColors;
uniform float line_width;
uniform int high_contrast;

#extension GL_OES_standard_derivatives : enable
float edgeFactorTri() {
    vec3 d = fwidth(vCenter.xyz);
    vec3 a3 = smoothstep(vec3(0.0), d * line_width, vCenter.xyz);
    return min(min(a3.x, a3.y), a3.z);
}

void main() {
    float gray = dot(vColors.rgb, vec3(0.299, 0.587, 0.114));

    // Shift to get rid of "medium" values.
    gray -= 0.5;
    gray = sign(gray) * clamp(abs(gray), 0.4, 0.5) + 0.5;

    gray = mix(0.5, gray, float(high_contrast));

    gl_FragColor.rgb = mix(
        // Line color - choose if high-contrast or not.
        vec3(1.0 - gray),
        // Background colormap color
        vColors.rgb,
        max(edgeFactorTri(), (1.0 - line_width)));
    gl_FragColor.a = vColors.a;
}