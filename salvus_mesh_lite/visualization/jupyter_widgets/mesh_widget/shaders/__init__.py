#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
import salvus_mesh_lite


def get_shader_material():
    import pythreejs as p3js  # NOQA

    shader_dir = (salvus_mesh_lite.__module_root__ / "visualization" /
                  "jupyter_widgets" / "mesh_widget" / "shaders")

    with open(shader_dir / "vertex_shader.glsl", "rt") as fh:
        vertex_shader = fh.read()

    with open(shader_dir / "fragment_shader.glsl", "rt") as fh:
        fragment_shader = fh.read()

    return p3js.ShaderMaterial(
        uniforms=dict(
            # Set the initial line width.
            line_width={"value": 1.5},
            # And turn off high-contrast mode as it can be confusing.
            high_contrast={"value": 0},
            **p3js.UniformsLib["common"]
        ),
        vertexShader=vertex_shader,
        fragmentShader=fragment_shader,
        # We don't have a ton of vertices here so this is a good idea.
        side="DoubleSide",
        # We need derivatives but this extension always exists.
        extensions={
            "derivatives": True
        }
    )