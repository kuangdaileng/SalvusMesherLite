#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
import json

import numpy as np

from . import __module_root__


with open(__module_root__ / "data" / "local_refinements.json", "r") as fh:
    __d = json.load(fh)

QUAD_ORDER_SALVUS = __d["QUAD_ORDER_SALVUS"]
HEX_ORDER_SALVUS = __d["HEX_ORDER_SALVUS"]
refinement_scheme = __d["refinement_scheme"]

# Patch the keys.
for key, value in list(refinement_scheme.items()):
    new_key = list(key.split("__"))
    new_key[0] = int(new_key[0])

    # Build up the arrays again...
    def _c(v):
        if not isinstance(v, list) or not isinstance(v[0], list):
            return v
        else:
            return [np.array(_i) for _i in v]
    refinement_scheme[tuple(new_key)] = tuple([_c(v) for v in value])
    del refinement_scheme[key]

del __d