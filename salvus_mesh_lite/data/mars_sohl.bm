# Example Polynomial Model Input File for AXISEM 2.0
NAME                mars_sohl

# is this model defined by multiple layers [layer] or polynomials [poly]
MODEL_TYPE          poly

# if anelastic, QMU an QKAPPA are defined, default: False
ANELASTIC           T

# reference frequency at which the velocities are defined in Hz, default: 1.
REFERENCE_FREQUENCY 1.

# if anisotropic, velocitities are defined as VPV, VPH, VSV, VSH and ETA is
# provided. If false, only VP and VS are needed. default: False
ANISOTROPIC         F

# number of regions in which the model is described by polynomials
NREGIONS            8

# radii of discontinuties, for whole planets starts from the center (0.0) and includes
# the free surface. This should hence be NREGIONS + 1 floats
DISCONTINUITIES     3390 3280 3055.5 2908 2360 2033 1468 515 0

# index of the Moho discontinuity from the top (0 = surface)
MOHO_IDX            1

# index of the discontinuity where Moho topography is compenstated from the top (0 = surface)
MOHO_COMP_IDX       2

# maximum polynomial degree throughout the whole model, default: 3
MAX_POLY_DEG        3

# prefactor to scale the radius before evaluating the polynomial, default:
# radius of the planet
SCALE               3390

# Are the units given in meters and meters per second?
# Allowed values:
#    m - lengths in m,  velocities in m/s,  density in kg/m^3
#   km - lengths in km, velocities in km/s, density in g/cm^3 
UNITS               km

# Indentations by at least two blanks are necessary.
# Assuming to have same order as DISCONTINUITIES (from center to the surface or vice versa)
RHO
  -36.937  120.62   -121.40    40.519
   2.2095  1.7831    1.2168   -1.7209 
   13.572 -34.678    40.167   -15.572
   4.1601 -0.8166    0.4844   -0.4175
   4.4067 -0.58162   0.17083  -0.30264
   4.5706 -0.5184   -0.03219  -0.13738
   7.2942 -0.0134654 -1.6906  -0.4225
   7.2942 -0.0134654 -1.6906  -0.4225

VP
  50.537   -123.68    123.81 -43.012
  14.372   -13.395    13.353 -6.3398
  15.559   -17.460    18.004 -8.1157
  11.9300  -4.8826    3.5016 -2.5538
  11.8365  -4.1713    3.1366 -2.5691
  11.4166  -0.90421  -2.6380  0.94287
  6.5395   -0.0225299 -2.3767 -0.72716
  6.5395   -0.0225299 -2.3767 -0.72716 

# Fluid is detected by a single 0. value
VS
  -43.819  148.53 -151.21 50.526
   8.6113 -11.932 14.301 -6.5441
   22.395 -57.011 63.447 -24.406
   6.5847 -2.5543 1.6024 -1.2675
   6.5172 -1.8055 0.8080 -0.95676
   6.7644 -2.3139 1.7809 -1.5312
   0.0
   2.4

QKAPPA
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  1327.7

QMU
  600.0
  600.0
  80.0
  143.0
  312.0
  312.0
  0.0
  84.6
