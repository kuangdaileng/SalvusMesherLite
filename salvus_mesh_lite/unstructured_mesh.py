#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
from collections import OrderedDict
import copy
import ctypes as C
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import multiprocessing
import os
import pathlib
from scipy.interpolate import RectBivariateSpline, LinearNDInterpolator, \
    CloughTocher2DInterpolator, RectSphereBivariateSpline
# from scipy.interpolate import SmoothSphereBivariateSpline, \
#     LSQSphereBivariateSpline
from scipy.spatial import cKDTree
from typing import Optional, Tuple, Union
from warnings import warn

from .connectivity import connectivity_2D, connectivity_3D
from .global_numbering import unique_points, compress_points_connectivity


from .models_1D import ELLIPSOIDS
from . import elements as elem


from .helpers import load_lib

lib = load_lib()



class UnstructuredMesh(object):

    @classmethod
    def from_exodus(cls, filename: str):

        from pyexodus import exodus

        read_mesh = exodus(filename)
        points = np.array(read_mesh.get_coords(), dtype=np.float64).transpose()
        if (points[:, 2] == 0).all():
            points = points[:, :2]
        connectivity = np.array(read_mesh.get_elem_connectivity(1)[0] - 1,
                                dtype=np.int64)

        um = UnstructuredMesh(points=points, connectivity=connectivity)

        try:
            for name in read_mesh.get_node_variable_names():
                um.attach_field(
                        name, read_mesh.get_node_variable_values(name, 1))
        except KeyError:
            pass

        try:
            for name in read_mesh.get_element_variable_names():
                um.attach_field(
                        name, read_mesh.get_element_variable_values(
                            1, name, 1))
        except KeyError:
            pass

        try:
            for name in read_mesh.get_global_variable_names():
                um.attach_global_variable(
                        name, read_mesh.get_global_variable_values(name))
        except KeyError:
            pass

        try:
            for name, id in zip(
                    read_mesh.get_side_set_names(),
                    read_mesh.get_side_set_ids()):
                set = read_mesh.get_side_set(id)
                um.side_sets[name] = (set[0] - 1, set[1] - 1)
        except KeyError:
            pass

        return um

    @classmethod
    def from_h5(cls, filename: str):

        with h5py.File(filename, 'r') as read_mesh:
            points = read_mesh['/MODEL/coordinates'][:]

            nelem, nodes_per_element, ndim = points.shape

            connectivity = np.arange(nelem * nodes_per_element).reshape(
                nelem, nodes_per_element)

            points, global_ids = unique_points(points.reshape(
                (nelem * nodes_per_element, ndim)))

            connectivity = global_ids[connectivity]

            um = UnstructuredMesh(points=points, connectivity=connectivity,
                                  tensorized=True)

            # @TODO: node variables > not yet in h5

            # element variables
            if "/MODEL/element_data" in read_mesh:
                data = read_mesh['/MODEL/element_data'][:]
                dimstr = read_mesh['/MODEL/element_data'].dims[1].label
                names = [s for s in dimstr[2:-2].split(' | ') if not s == '']
                for i, name in enumerate(names):
                    um.attach_field(name, data[:, i])

            # element nodal variables
            if "/MODEL/data" in read_mesh:
                ngll = int(round(nodes_per_element ** (1. / ndim)))
                data = read_mesh['/MODEL/data'][:]
                dimstr = read_mesh['/MODEL/data'].dims[1].label
                names = [s for s in dimstr[2:-2].split(' | ') if not s == '']
                for i, name in enumerate(names):
                    for j in range(ngll ** ndim):
                        um.attach_field(name + '_%d' % j, data[:, i, j])

            # global variables
            for name, data in read_mesh['/MODEL'].attrs.items():
                um.attach_global_variable(name, data)

            # @TODO: global strings > not yet in h5

            # side sets
            # mapping needed for the different side set ordering in the solver
            if ndim == 3:
                side_map = np.zeros(8)
                side_map[[2, 4, 3, 5, 0, 1]] = np.arange(6)
            else:
                side_map = np.zeros(4)
                side_map[[0, 2, 1, 3]] = np.arange(4)

            if "SIDE_SETS" in read_mesh:
                names = list(read_mesh['SIDE_SETS'].keys())
                for name in names:
                    element_ids = \
                        read_mesh['SIDE_SETS/' + name + '/elements'][:]
                    side_ids = read_mesh['SIDE_SETS/' + name + '/sides'][:]
                    um.define_side_set(name, element_ids=element_ids,
                                       side_ids=side_map[side_ids])

        return um

    def __init__(self, points, connectivity, element_type=1, scale=1.,
                 tensorized=False):

        self.points = points.copy()
        self.connectivity = connectivity.copy()

        if isinstance(element_type, int):
            self.element_type = np.ones(self.nelem, dtype='int') * element_type
        else:
            self.element_type = np.array(element_type)

        self.scale = scale
        self.tensorized = tensorized

        self._reset()

    def _reset(self):
        self.global_variables = OrderedDict()
        self.global_strings = OrderedDict()
        self.global_arrays = OrderedDict()
        self._reset_fields()

    def _reset_fields(self):
        self.elemental_fields = OrderedDict()
        self.nodal_fields = OrderedDict()
        self.side_sets = OrderedDict()
        self.topography = OrderedDict()

    def __add__(self, other, make_unique_points=True):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def copy(self):
        return self.__copy__()

    def __deepcopy__(self, memo):
        return self.__copy__()

    def __copy__(self):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    @property
    def nelem(self):
        '''
        Number of elements in the mesh.
        '''
        return self.connectivity.shape[0]

    @property
    def npoint(self):
        '''
        Number of points / nodes in the mesh
        '''
        return self.points.shape[0]

    @property
    def ndim(self):
        '''
        Number of space dimensions. For now either 2 or 3.
        '''
        return self.points.shape[1]

    @property
    def nodes_per_element(self):
        '''
        Number of nodes per element, e. g. 4 for Quads and 8 for Hex
        '''
        return self.connectivity.shape[1]

    @property
    def edges_per_element(self):
        '''
        Number of edges per element, e. g. 4 for Quads and 12 for Hex
        '''
        return len(elem.edges[self.ndim, self.nodes_per_element])

    def get_element_centroid(self, spherical=False):
        '''
        Compute the centroids of all elements on the fly from the nodes of the
        mesh. Usefull to determine which domain in a layered medium an element
        belongs to or to compute elemtal properties from the model.
        '''
        centroid = np.zeros((self.nelem, self.ndim))

        lib.centroid(self.ndim, self.nelem, self.nodes_per_element,
                     self.connectivity, self.points, centroid)

        if spherical:
            centroid_radius = np.zeros(self.nelem)
            lib.centroid_radius(self.ndim, self.nelem, self.nodes_per_element,
                                self.connectivity, self.points,
                                centroid_radius)
            centroid *= (centroid_radius / (centroid ** 2).sum(axis=-1) **
                         0.5)[:, np.newaxis]

        return centroid

    def get_element_centroid_radius(self):
        '''
        Compute the centroids of all elements on the fly from the nodes of the
        mesh. Usefull to determine which domain in a layered medium an element
        belongs to or to compute elemtal properties from the model.
        '''
        centroid_radius = np.zeros(self.nelem)
        lib.centroid_radius(self.ndim, self.nelem, self.nodes_per_element,
                            self.connectivity, self.points,
                            centroid_radius)

        return centroid_radius

    def get_element_directions(self):
        '''
        compute vectors that point into the reference coordinate direction in
        the center of the element.
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def get_side_set(self, name):
        '''
        return side set as a python set
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def attach_global_variable(self, name, data):
        '''
        store global mesh variable.

        :param name: name of the variable
        :type name: string
        :param data: data to store
        :type data: float, string or numpy.ndarray
        '''
        if isinstance(data, str):
            self.global_strings[name] = data
        elif isinstance(data, float) or isinstance(data, np.float64):
            self.global_variables[name] = data
        elif type(data) is np.ndarray:
            self.global_arrays[name] = data
        else:  # pragma: no cover
            raise TypeError(
                'Global Variables can only be float, string or numpy array.')

    def attach_field(self, name, data):
        '''
        store data on either the elements or the nodes (determined by the size
        of data).

        :param name: name of the field
        :type name: string
        :param data: data to store
        :type data: 1D numpy array, with size of either nelem or noint
        '''
        if data.size == self.npoint:
            self.nodal_fields[name] = data
        elif data.size == self.nelem:
            self.elemental_fields[name] = data
        else:  # pragma: no cover
            raise ValueError('Shape matches neither the nodes nor the '
                             'elements')

    def define_side_set(self, name, element_ids=None, side_ids=None,
                        side_set=None):
        '''
        define a set of edges (2D) or faces (3D) with a name.

        Either provide element_ids AND side_set, or only side_ids

        :param name: name of the side set
        :type name: string
        :param element_ids: element ids of the sides
        :type element_ids: 1D numpy integer array
        :param side_ids: side ids of the sides
        :type element_ids: 1D numpy integer array, same shape as element_ids
        :param side_set: set of tuples with element id and side id
        :type element_ids: set of tuples (int, int)
        '''
        if element_ids is not None and side_ids is not None:
            if not element_ids.shape == side_ids.shape:  # pragma: no cover
                raise ValueError('Shape mismatch')

        elif side_set is not None:
            element_ids, side_ids = np.asarray(list(side_set)).T
        else:  # pragma: no cover
            raise ValueError('provide the side set either as a python set or '
                             'two numpy arrays')

        self.side_sets[name] = (element_ids, side_ids)

    @property
    def nnodal_fields(self):
        return len(self.nodal_fields)

    @property
    def nelemental_fields(self):
        return len(self.elemental_fields)

    @property
    def nglobal_variables(self):
        return len(self.global_variables)

    @property
    def nglobal_strings(self):
        return len(self.global_strings)

    @property
    def nglobal_arrays(self):
        return len(self.global_arrays)

    @property
    def nside_sets(self):
        return len(self.side_sets)

    def rotate_coordinates(self, euler_angles):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def add_dem_2D(self, x, dem, y0=0., y1=np.infty, yref=None, kx=3, ky=1,
                   s=None, name=None):
        '''
        Add topography by vertically stretching the domain in the region [y0,
        y1] - points below y0 are kept fixed, points above y1 are moved as
        the DEM, points in between are interpolated.

        Usage: first call add_dem_2D for each boundary that is to be perturbed
            and finally call apply_dem to add the perturbation to the mesh
            coordinates.

        :param x: x coordinates of the DEM
        :type x: numpy array
        :param dem: the DEM
        :type dem: numpy array
        :param y0: vertical coordinate, at which the stretching begins
        :type y0: float
        :param y1: vertical coordinate, at which the stretching ends, can be
            infinity
        :type y1: float
        :param yref: vertical coordinate, at which the stretching ends
        :type yref: float
        :param kx: horizontal degree of the spline interpolation
        :type kx: integer
        :param ky: vertical degree of the spline interpolation
        :type ky: integer
        :param s: smoothing factor
        :type s: float
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def add_dem_3D(self, x, y, dem, z0=0., z1=np.infty, zref=None,
                   khorizontal=3, s=None, mode='cartesian', name=None):
        '''
        Add topography by vertically stretching the domain in the region [z0,
        z1] - points below z0 and above z1 are kept fixed, points in between
        are linearly interpolated.

        Usage: first call add_dem_3D for each boundary that is to be perturbed
            and finally call apply_dem to add the perturbation to the mesh
            coordinates.

        The DEM can be either structured or unstructured, this is determined
        from the shapes of x and y.

        See scipy.interpolate.RectBivariateSpline for structured Spline
        interpolation and scipy.interpolate.LinearNDInterpolator
        (khorizontal=1) or scipy.interpolate.CloughTocher2DInterpolator
        (khorizontal=3) for more information on the unstructured interpolation.

        :param x: x coordinates of the DEM (colatitude in spherical models in
            radians)
        :type x: numpy array, either 1D for structured data or 2D for
            unstructured data
        :param y: y coordinates of the DEM (longitude in spherical modes in
            radians)
        :type y: numpy array, either 1D for structured data or 2D for
            unstructured data
        :param dem: the DEM
        :type dem: numpy array
        :param z0: vertical coordinate, at which the stretching begins
        :type z0: float
        :param z1: vertical coordinate, at which the stretching ends, can be
            np.infty to just move all points above zref with the DEM
        :type z1: float
        :param zref: vertical coordinate, at which the stretching ends
        :type zref: float
        :param khorizontal: horizontal degree of the spline interpolation. For
            unstructured data either 1 or 3.
        :type khorizontal: integer
        :param s: smoothing factor
        :type s: float
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def add_ellipticity(self, ellipticity='WGS84'):
        '''
        Add ellipticity by radial stretching the domain.

        Usage: first call add_ellipticity and add_dem_2D/3D and finally call
        apply_dem to add the perturbation to the mesh coordinates.

        :parameter ellipticity: ellipticity as a function of radius
        :type ellipticity: string, float, array of floats or callback function
            to be called with the radius
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def apply_dem(self, names=None):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def find_side_sets(self, mode='cartesian', tolerance=1e-8):
        '''
        extract surfaces of a 3D box mesh. Needs to be called before appling
        the DEM or space mapping. Assuming 3D hexahedral meshes, where the
        surface elements are all quads or 2D quadrilateral meshes, where the
        surface elements are lines.
        '''
        if mode not in ['cartesian', 'spherical_full', 'spherical_chunk',
                        'spherical_chunk_z', 'spherical_full_axisem',
                        'spherical_smoothiesem']:  # pragma: no cover
            raise ValueError('unkown side set mode %s' % mode)

        if mode == 'cartesian':
            dim_map = ['x', 'y', 'z']

            for dim in np.arange(self.ndim):
                for l, minmax in enumerate([np.min, np.max]):
                    def distance(points):
                        return np.abs(points[:, dim] - minmax(points[:, dim]))

                    name = '%s%d' % (dim_map[dim], l)
                    self.find_side_sets_generic(name, distance,
                                                tolerance=tolerance)

        if mode in ['spherical_full', 'spherical_smoothiesem'] or \
           mode.startswith('spherical_chunk'):
            for l, minmax in enumerate([np.min, np.max]):
                # radial
                def distance(points):
                    r = (points ** 2).sum(axis=1) ** 0.5
                    return np.abs(r - minmax(r))

                name = 'r%d' % (l,)
                self.find_side_sets_generic(name, distance,
                                            tolerance=tolerance)

        if mode.startswith('spherical_chunk'):
            if self.ndim == 2:
                names = ('t',)
                idx_x = (0,)
                idx_z = (1,)
            elif self.ndim == 3:
                raise ValueError('This feature is not included in the free SalvusMesh version.')

            for l, minmax in enumerate([np.min, np.max]):
                for n, ix, iz in zip(names, idx_x, idx_z):
                    def distance(points):
                        # theta = atan(x / z)
                        c = np.arctan2(points[:, ix], points[:, iz])
                        return np.abs(c - minmax(c))

                    name = '%s%d' % (n, l)
                    self.find_side_sets_generic(name, distance,
                                                tolerance=tolerance)

        if mode == 'spherical_full_axisem':
            self.find_side_sets('spherical_full', tolerance)

            def distance(points):
                return np.abs(points[:, 0])

            name = 't0'
            self.find_side_sets_generic(name, distance, tolerance=tolerance)

    def find_side_sets_generic(self, name, distance, tolerance=1e-8,
                               attach_side_set=True):
        '''
        define side sets based on a callback function for the distance to that
        surface.
        '''
        # need to map the boundary elements to proper quads with correct node
        # ordering
        side_set_mask = elem.side_set_mask[(self.ndim, self.nodes_per_element)]

        # points on this boundary
        on_boundary_dim = distance(self.points) < tolerance

        # collect connectivity of this boundary
        masks = on_boundary_dim[self.connectivity[:, :]]

        # map to side definitions from exodus
        side_set_elems = []
        side_set_sides = []

        element_ids = np.arange(self.nelem)
        for i, qm in enumerate(side_set_mask):
            sl = np.all(masks == qm, axis=1)
            side_set_elems.append(element_ids[sl])
            side_set_sides.append(np.ones(sl.sum(), dtype='int') * i)

        side_set_elems = np.hstack(side_set_elems)
        side_set_sides = np.hstack(side_set_sides)

        if attach_side_set and len(side_set_elems) > 0:
            self.define_side_set(name, side_set_elems, side_set_sides)
        else:
            return side_set_elems, side_set_sides

    def find_surface(self, side_set_name='surface'):
        '''
        find the surface of the mesh, i.e. all element sides that don't touch
        another element
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def get_hierarchical_map(self, other, nneighbour=8,
                             check_other_is_refinement=True):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def tensorize(self, order=3, spherical=False, r0_spherical=0.,
                  r1_spherical=0.):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def convert_element_type(self, mode='QuadToTri', spherical=False,
                             r0_spherical=0., r1_spherical=0.,
                             tensor_node_locations=None, smoothiesem=False,
                             r0_cylindrical=0., r1_cylindrical=0.,
                             make_unique_points=True):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def refine_locally(self, mask, refinement_level=1, hierarchical_map=None,
                       refinement_style='unstable', spherical=False,
                       r0_spherical=0., r1_spherical=0.,
                       reinterpolate_nodal_fields=False):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def _get_suitable_cpu_count(self):
        """
        Try to get a sensible CPU count for the multiprocessing parts.
        """
        return max(1, min(multiprocessing.cpu_count(), self.nelem // 1000, 16))


    def extrude(self, offsets, scale=None, rotation=None, center=None):
        raise ValueError('This feature is not included in the free SalvusMesh version.')


    def extrude_side_set(self, side_set_elems, side_set_sides, offsets,
                         scale=None, rotation=None, center=None):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def apply_element_mask(self, mask, return_mask=False,
                           tolerance_decimals=8):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def split_nodes(self, nodes, elements):
        '''
        split the nodes and replace them in elements by the new node

        :param nodes: array of the nodes to be split
        :type name: numpy.ndarray of integer
        :param elements: list of arrays of the elements for each split node
        :type elements: list of numpy.ndarray of integer
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def map_to_sphere(self, min_longitude=0., max_longitude=20.,
                      min_colatitude=80., max_colatitude=100., min_radius=0.8,
                      max_radius=1.):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def compute_dt(self, vp, courant_number=1., min_gll_point_distance=1.,
                   fast=True, return_hmin_elemnodes=False):
        """
        estimate the time step based on the Courant criterion and the
        edgelengths of the elements.

        :param vp: scaled p-wave velocity for each element
        :type vp: array of float
        :param courant_number: Courant number
        :type courant_number: float
        """
        if fast:
            hmin = self._hmin()

        else:
            raise ValueError('This feature is not included in the free SalvusMesh version.')

        hmin *= min_gll_point_distance

        # for plotting
        dt_elem = courant_number * hmin / vp

        # actual dt
        dt = float(dt_elem.min())

        if not fast and return_hmin_elemnodes:
            return dt, dt_elem, hmin_elemnodes
        else:
            return dt, dt_elem

    def compute_resolved_frequency(self, vmin, elements_per_wavelength):
        """
        estimate the highest resolved frequency:
            vmin / hmax / elements_per_wavelength

        :param vmin: scaled minimum velocity for each element
        :type vmin: array of float
        :param elements_per_wavelength: number of elements needed to resolve
            one wavelength
        :type elements_per_wavelength: float
        """
        hmax = self._hmax()

        resolved_frequency = vmin / hmax / elements_per_wavelength

        return float(resolved_frequency.min()), resolved_frequency

    def compute_mesh_quality(self, quality_measure='edge_aspect_ratio',
                             **kwargs):
        """
        compute mesh quality
        :param quality_measure: one of 'edge_aspect_ratio' or
            'equiangular_skewness'
        :type quality_measure: string
        """
        if quality_measure == 'edge_aspect_ratio':
            hmin = self._hmin()
            hmax = self._hmax()

            quality = hmax / hmin

        elif quality_measure == 'hmax_over_hmax_in':
            hmax_in = kwargs.get('hmax_in')
            hmax = self._hmax()

            quality = hmax / hmax_in

        elif quality_measure == 'equiangular_skewness':
            angles = elem.angles[(self.ndim, self.nodes_per_element)]
            theta_e = elem.theta_e[(self.ndim, self.nodes_per_element)]

            theta_min = np.zeros(self.nelem)
            theta_max = np.zeros(self.nelem)
            theta_min[:] = np.inf

            theta = np.empty(self.nelem)
            for idx in angles:
                lib.angle(self.ndim, self.nelem, self.nodes_per_element,
                          idx[0], idx[1], idx[2], self.connectivity,
                          self.points, theta)

                theta_min = np.minimum(theta_min, theta)
                theta_max = np.maximum(theta_max, theta)

            quality = np.maximum((theta_max - theta_e) / (np.pi - theta_e),
                                 (theta_e - theta_min) / theta_e)

        else:  # pragma: no cover
            raise ValueError('Unknown quality measure %s' % (quality_measure))

        return quality

    def _hmin(self):
        hmin = np.empty(self.nelem)
        hmin[:] = np.inf
        edges = np.array(elem.edges[(self.ndim, self.nodes_per_element)])
        lib.hmin(self.ndim, self.nelem, self.npoint, edges.shape[0],
                 self.nodes_per_element, self.connectivity, edges, self.points,
                 hmin)
        return hmin

    def _hmax(self):
        hmax = np.zeros(self.nelem)
        edges = np.array(elem.edges[(self.ndim, self.nodes_per_element)])
        lib.hmax(self.ndim, self.nelem, self.npoint, edges.shape[0],
                 self.nodes_per_element, self.connectivity, edges, self.points,
                 hmax)
        return hmax

    def _jacobian_at_nodes(self):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def resort_elements_kdtree(self):
        """
        resort elements to increase likelihood that elements that are close in
        the connecivity array are also close in space
        """
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_exodus(self,
                     filename: Union[str, pathlib.Path],
                     overwrite: bool = True,
                     title: Optional[str] = None,
                     compression: Optional[Tuple[str, int]] = None):
        """
        write mesh to exodus file

        :param filename: filename of the meshfile
        :type filename: string
        :param overwrite: overwrite if the file already exists
        :type overwrite: boolean
        :param title: title of the mesh, defaults to filename
        :type title: string
        :param compression: Turn on compression. Pass a tuple of
            ``(method, option)``, e.g. ``("gzip", 2)``. Slows down writing a
            lot but the resulting files are potentially much smaller.
        :type compression: tuple
        """
        filename = str(filename)

        from pyexodus import exodus
        is_pyexodus = True

        # remove file, if it exists
        if overwrite:
            try:
                os.remove(filename)
            # if the file does not exist, do nothing
            except OSError:
                pass

        if title is None:
            title = filename.split('.')[0]

        # add a default reference frame
        if 'reference_frame' not in self.global_strings:
            self.global_strings['reference_frame'] = 'cartesian'

        kwargs = {'mode': 'w', 'title': title, 'array_type': 'numpy',
                  'numDims': self.ndim, 'numNodes': self.npoint,
                  'numElems': self.nelem, 'numBlocks': 1, 'numNodeSets': 0,
                  'numSideSets': self.nside_sets}

        if is_pyexodus:
            kwargs['compression'] = compression

        e = exodus(filename, **kwargs)

        e.put_info_records(["%s = %s" % t for t in
                            self.global_strings.items()])

        if self.ndim == 2:
            e.put_coords(self.points[:, 0] * self.scale,
                         self.points[:, 1] * self.scale,
                         np.zeros(self.npoint))
        elif self.ndim == 3:
            raise ValueError('This feature is not included in the free SalvusMesh version.')

        name = elem.name[(self.ndim, self.nodes_per_element)]
        e.put_elem_blk_info(1, name, self.nelem, self.nodes_per_element, 0)

        if is_pyexodus:
            e.put_elem_connectivity(1, self.connectivity, shift_indices=1)
        # Memory intensive!
        else:  # pramga: no cover
            e.put_elem_connectivity(1, self.connectivity.ravel() + 1)

        # we store the variables in the results section of the exodus file,
        # hence need to define a time step
        e.put_time(1, 0.)

        # write global variables
        e.set_global_variable_number(self.nglobal_variables)
        for i, (name, data) in enumerate(sorted(self.global_variables.items(),
                                                key=lambda x: x[0])):
            e.put_global_variable_name(name, i + 1)
            e.put_global_variable_value(name, 1, data)

        # write elemental fields
        e.set_element_variable_number(self.nelemental_fields)
        for i, (name, data) in enumerate(sorted(self.elemental_fields.items(),
                                                key=lambda x: x[0])):
            e.put_element_variable_name(name, i + 1)
            e.put_element_variable_values(1, name, 1, data)

        # write nodal fields
        e.set_node_variable_number(self.nnodal_fields)
        for i, (name, data) in enumerate(sorted(self.nodal_fields.items(),
                                                key=lambda x: x[0])):
            e.put_node_variable_name(name, i + 1)
            e.put_node_variable_values(name, 1, data)

        # write side sets
        for i, (name, (elements, sides)) in enumerate(
             sorted(self.side_sets.items(), key=lambda x: x[0])):
            if elements.size > 0:
                e.put_side_set_params(i + 1, elements.size, 0)
                e.put_side_set(i + 1, elements + 1, sides + 1)
                e.put_side_set_name(i + 1, name)

        e.close()

        # reopen the file to attach global array variables
        if self.nglobal_arrays > 0:
            # make sure we can work with pyexodus as well as the legacy library
            # h5netcdf can't write the netcdf3 files!
            if is_pyexodus:
                import h5netcdf.legacyapi as netCDF4
            else:
                import netCDF4

            nc = netCDF4.Dataset(filename, 'a')

            def _put_global_array_to_exodus(var_name, var_data):
                dims = ()
                for i in np.arange(var_data.ndim):
                    dim_name = var_name + '_dim_%d' % i
                    nc.createDimension(dim_name, var_data.shape[i])
                    dims += (dim_name,)
                nc.createVariable(var_name, var_data.dtype, dims,
                                  zlib=compression is not None)
                nc[var_name][:] = var_data

            # write global arrays
            for i, (name, data) in enumerate(sorted(self.global_arrays.items(),
                                                    key=lambda x: x[0])):
                _put_global_array_to_exodus(name, data)

            nc.close()

    def write_obj_file(self, filename,
                       side_sets=['x0', 'x1', 'y0', 'y1', 'z0', 'z1']):
        '''
        Write the mesh to a wavefront OBJ file.

        Useful as it can be imported by essentially every 3D program. For a
        3D mesh it only writes the hull.
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_legacy_vtk(self, filename, volume=False,
                         side_sets=['x0', 'x1', 'y0', 'y1', 'z0', 'z1']):
        '''
        Write the mesh to a legacy vtk file, meant for visulization with the
        GUI. In 3D, only writes the hull.
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_binary_vtk(self, filename):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_vtp(self, filename,
                  side_sets=['x0', 'x1', 'y0', 'y1', 'z0', 'z1']):
        '''
        Write the mesh to a vtp file, meant for visulization with the
        GUI. In 3D, only writes the hull.
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def _build_side_set_connectivity(
            self, side_sets=['x0', 'x1', 'y0', 'y1', 'z0', 'z1'],
            return_global_element_ids=False):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_xdmf_subcon_tensorized(self, filename):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_xdmf_polyline_tensorized(self, filename):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_h5_tensorized(self, filename, write_connectivity=True,
                            write_duplicated_points=True):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def write_h5_tensorized_model(self, filename, datatype=np.float64,
                                  compression=None,
                                  add_visualization_data=True,
                                  write_chunk_size=10000, overwrite=True):
        '''
        :param compression: Turn on compression. Pass a tuple of
            ``(method, option)``, e.g. ``("gzip", 2)``. Slows down writing a
            lot but the resulting files are potentially much smaller.
        :type compression: tuple
        '''
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def plot(self, show=True, linewidths=0.5, scatter=False,
             elemental_data=None, element_node_data=None, figure=None,
             clim=None):

        if figure is None:
            figure = plt.figure()
            ax = plt.axes() if self.ndim == 2 else plt.axes(projection='3d')
            ax.set_aspect('equal')
        else:
            ax = figure.gca()

        if self.ndim == 2:
            patches = []

            if self.tensorized:
                n = int(round(self.nodes_per_element ** 0.5))
                plot_line = np.r_[np.arange(n),
                                  np.arange(1, n) * n + n - 1,
                                  (np.arange(n - 1) + n ** 2 - n)[::-1],
                                  (np.arange(1, n - 1) * n)[::-1]]
            else:
                plot_line = elem.plot_line[(self.ndim, self.nodes_per_element)]

            for ielem in np.arange(self.nelem):
                points = self.points[self.connectivity[ielem, :]]
                points = points[plot_line]
                polygon = Polygon(points, closed=True, fill=False)
                patches.append(polygon)

            # facecolor = None if element_node_data is None else 'none'
            facecolor = 'none'
            p = PatchCollection(patches, edgecolor='k', lw=linewidths,
                                facecolor=facecolor)

            if elemental_data is not None:
                p.set_array(np.array(elemental_data))
                plt.colorbar(p, orientation="horizontal")
                if clim is not None:
                    p.set_clim(clim)

            elif element_node_data is not None:
                if not self.tensorized:
                    raise ValueError
                if clim is None:
                    vmin = element_node_data.min()
                    vmax = element_node_data.max()
                else:
                    vmin, vmax = clim

                n = int(round(self.nodes_per_element ** 0.5))
                idx = np.arange(self.nodes_per_element).reshape((n, n))

                for e in range(self.nelem):
                    _p = self.points[self.connectivity[e]]
                    _p = _p[idx, :]
                    cm = ax.pcolormesh(_p[..., 0], _p[..., 1],
                                       element_node_data[e, idx],
                                       vmin=vmin, vmax=vmax, shading='gouraud',
                                       cmap='seismic')

                plt.colorbar(cm)

            ax.add_collection(p)

            if scatter:
                plt.scatter(self.points[:, 0], self.points[:, 1], s=50, c='r')

            # auto scaling of the axis seem not to work with
            # patchcollection only
            epsx = self.points[:, 0].ptp() / 20
            epsy = self.points[:, 1].ptp() / 20
            plt.xlim(self.points[:, 0].min() - epsx,
                     self.points[:, 0].max() + epsx)
            plt.ylim(self.points[:, 1].min() - epsy,
                     self.points[:, 1].max() + epsy)

        elif self.ndim == 3:

            lid = elem.plot_line[(self.ndim, self.nodes_per_element)]
            for ielem in np.arange(self.nelem):
                p = self.points[self.connectivity[ielem, :]]
                ax.plot3D(
                    p[lid, 0], p[lid, 1], p[lid, 2],
                    color="b")

            if scatter:
                ax.scatter(self.points[:, 0], self.points[:, 1],
                           self.points[:, 2], s=50, c='r')

        else:  # pragma: no cover
            raise NotImplementedError()

        if show:  # pragma: no cover
            plt.show()
        else:
            return figure

    def plot_side_sets(self, show=True, linewidth=0.5, figure=None,
                       side_sets=None, color='b', linestyle='-'):
        raise ValueError('This feature is not included in the free SalvusMesh version.')

    def plot_quality(self, quality_measure='edge_aspect_ratio', show=True,
                     hist_kwargs={}, compute_quality_kwargs={}):
        figure = plt.figure()

        quality = self.compute_mesh_quality(
            quality_measure, **compute_quality_kwargs)

        plt.hist(quality, **hist_kwargs)
        plt.title('max = %4.2f, min = %4.2f' % (quality.max(), quality.min()))
        plt.xlabel(quality_measure)
        plt.ylabel('frequency')

        if show:  # pragma: no cover
            plt.show()
        else:
            return figure

    def _repr_html_(self):  # NOQA
        from IPython.display import display
        from .visualization.jupyter_widgets.mesh_widget.mesh_widget \
            import MeshWidget
        display(MeshWidget(mesh=self).main_widget)


def _scatter(ref_mask, start, stop, nelem, edges_per_element, connectivity,
             edges, ref_edges):
    ref_mask = to_numpy(ref_mask, np.bool,
                        (nelem, edges_per_element))

    elem_edges = np.sort(connectivity[:, edges], axis=-1)
    for ielem in range(start, stop):
        for iedge in range(edges_per_element):
            ref_mask[ielem, iedge] = (elem_edges[ielem, iedge, 0],
                                      elem_edges[ielem, iedge, 1]) in ref_edges

    return ref_mask


def to_numpy(raw_array, dtype, shape):
    data = np.frombuffer(raw_array, dtype=dtype)
    return data.reshape(shape)


def _element_loop(start, stop, point_offsets, conn_offsets, node_rotations,
                  rotation, self_points, self_connectivity, point_templates,
                  connect_templates, template_map, smoothiesem, new_points,
                  spherical, r0_spherical, r1_spherical,
                  new_hierarchical_map, hierarchical_map, new_connectivity,
                  nodes_per_element, ndim, nnew_points, nnew_elements):
    new_points = to_numpy(new_points, np.float64, (nnew_points, ndim))
    new_connectivity = to_numpy(new_connectivity, np.int64,
                                (nnew_elements, nodes_per_element))
    new_hierarchical_map = to_numpy(new_hierarchical_map, np.int64,
                                    (nnew_elements, ))

    elem_points = np.empty((nodes_per_element, ndim))

    for ielem in range(start, stop):

        point_offset = point_offsets[ielem]
        conn_offset = conn_offsets[ielem]

        # rotated elementvertices so we can ignore the rotation when
        # pluggin in the templates
        elem_points[node_rotations[rotation[ielem]]] = \
            self_points[self_connectivity[ielem, :]]

        pt = point_templates[template_map[ielem]]
        ct = connect_templates[template_map[ielem]]

        # assume the T0 template has the same nodes and node ordering as
        # the original element (see local_refinements.py)
        if template_map[ielem] == 0:
            new_points[point_offset:point_offset+len(pt), :] = \
                elem_points[:]

        else:
            # get new nodes, mapped to the original element
            if smoothiesem:
                r = (elem_points[:, :] ** 2).sum(axis=1) ** 0.5
                rr = (elem_points[:, :2] ** 2).sum(axis=1) ** 0.5
                theta = np.arctan2(rr, elem_points[:, 2])
                phi = np.arctan2(elem_points[:, 1], elem_points[:, 0])

                rtp = np.c_[r, theta, phi]

                # ensure elements don't cross the periodicity boundary in
                # phi assume no element spans more than 180 degree
                on_boundary = (np.any(rtp[:, 2] <= - np.pi / 2) *
                               np.any(rtp[:, 2] >= np.pi / 2))
                if on_boundary:
                    mask = rtp[:, 2] < 0
                    rtp[mask, 2] += np.pi * 2

                # interpolate in r, theta, phi
                new_rtp = coordinate_transform(pt, rtp, spherical=False)
                r = new_rtp[..., 0]
                theta = new_rtp[..., 1]
                phi = new_rtp[..., 2]

                # convert back to cartesian
                new_points1 = np.zeros_like(new_rtp)
                new_points1[..., 0] = r * np.sin(theta) * np.cos(phi)
                new_points1[..., 1] = r * np.sin(theta) * np.sin(phi)
                new_points1[..., 2] = r * np.cos(theta)
            else:
                new_points1 = coordinate_transform(pt, elem_points, spherical)

                if spherical:
                    new_points2 = coordinate_transform(pt, elem_points,
                                                       spherical=False)

                    r3 = (new_points1[:, :] ** 2).sum(axis=1) ** 0.5
                    w = (1. - np.cos(((r3 - r0_spherical) /
                                     (r1_spherical - r0_spherical)) *
                                     np.pi / 2.))
                    w[r3 > r1_spherical] = 1.
                    w[r3 < r0_spherical] = 0.

                    new_points1 = w[:, np.newaxis] * new_points1 + \
                        (1. - w[:, np.newaxis]) * new_points2

            new_points[point_offset:point_offset+len(pt), :] = new_points1

        # get new connectivity
        new_connectivity[conn_offset:conn_offset+len(ct)] = \
            ct[:, :] + point_offset

        # keep track of the parent elements
        new_hierarchical_map[conn_offset:conn_offset+len(ct)] = \
            hierarchical_map[ielem]