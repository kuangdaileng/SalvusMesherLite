# -*- coding: utf-8 -*-
"""
:copyright:
    Mondaic AG (www.mondaic.com), 2018
"""
from libcpp.string cimport string
from libcpp cimport bool


cdef extern from "LicensingClient/license_client.h" namespace "Salvus":
    cdef string AcquireLicense(
        string product,
        string licenseVersion,
        string publicKeyHex,
        int instances,
        int durationInSeconds,
        bool verbose)

    cdef void ReleaseLicense(string product,
                             string licenseVersion,
                             string publicKeyHex,
                             string originalResponse,
                             bool verbose)


# Kind of hacked together to get the macro expansion to work with returned
# struct.
# But if it works its not stupid.
cdef extern from "LicensingClient/obfuscation.h":
    cdef cppclass MetaString:
        char* decrypt()
    cdef MetaString DEF_OBFUSCATED_STR_WITH_LENGTH(char*, int)
